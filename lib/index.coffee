primitiveTypes = [String, Number, Boolean, Date, Undefined]
Undefined = ->
Object.defineProperty Undefined.prototype, 'valueOf',
  enumerable: false
  get: -> -> undefined
Object.defineProperty Undefined.prototype, 'toJSON',
  enumerable: false
  get: -> -> undefined

hasOwn = {}.hasOwnProperty

forOwn = (obj, f) ->
  for prop of obj
    if hasOwn.call(obj, prop)
      f prop, obj[prop]

helpers =
  isPlainObject: require 'is-plain-object'
  isObject: require 'is-object'
  isGeneratorFunction: require 'is-generator-function'
  isFunction: (obj) ->
    return not not (obj and obj.constructor and obj.call and obj.apply)
  isArray: (obj) ->
    return obj instanceof Array

  Undefined: Undefined

  detectPrimitiveType: (val)->
    return val?.constructor or Undefined

  isPrimitive: (obj) ->
    type = @detectPrimitiveType(obj)
    return primitiveTypes.indexOf(type) isnt -1

  isUndefined: (obj)->
    return obj is Undefined

  hasOwn: hasOwn
  forOwn: forOwn

  extend: (what, byWhat)->
    return if not (what? and byWhat?)
    for k,v of byWhat
      what[k] = v

  delay: (interval, fn)->
    setTimeout fn, interval

  interval: (interval, fn)->
    setIntraval fn, interval

  format:
    currency: (val)->
      formatted =  parseInt(val).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,')
      formatted = formatted.slice(0, formatted.length - 3)
      return formatted

  guid: ->
    s4 = ->
      Math.floor((1 + Math.random()) * 0x10000).toString(16).substring 1
    s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4()

  validators:
    validateEmail: (value) ->
      if not value then return
      regEmail = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
      if not regEmail.test then throw new Error "INCORRECT_EMAIL"

    validateUrl: (value) ->
      if not value[0]? then return true
      regUrl = /^(?:(?:https?|ftp):\/\/)(?:\S+(?::\S*)?@)?(?:(?!10(?:\.\d{1,3}){3})(?!127(?:\.\d{1,3}){3})(?!169\.254(?:\.\d{1,3}){2})(?!192\.168(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]+-?)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/[^\s]*)?$/i
      if not regUrl.test then throw new Error "INCORRECT_URL"

    validatePhoneNumber: (value) ->
      if not value then return
      normalizedNumber = value.replace(/[^\d]/g, '')
      if normalizedNumber.length != 11 or normalizedNumber.charAt(0) != '7'
        throw new Error "INCORRECT_PHONE_NUMBER"

    validateIP: (value) ->
      true

  cookie:
    get: (name) ->
      matches = document.cookie.match(new RegExp('(?:^|; )' + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + '=([^;]*)'))
      if matches then decodeURIComponent(matches[1]) else undefined

    set: (name, value, options) ->
      options = options or {}
      expires = options.expires
      if typeof expires == 'number' and expires
        d = new Date
        d.setTime d.getTime() + expires * 1000
        expires = options.expires = d
      if expires and expires.toUTCString
        options.expires = expires.toUTCString()
      value = encodeURIComponent(value)
      updatedCookie = name + '=' + value
      for propName of options
        updatedCookie += '; ' + propName
        propValue = options[propName]
        if propValue != true
          updatedCookie += '=' + propValue
      document.cookie = updatedCookie

    delete: (name) ->
      helpers.cookie.set name, '', expires: -1

module.exports = helpers